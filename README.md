# Mini panels token #

This module provides tokens for embeding mini panels.

`[panels_mini:my_mini_panel]`
> This token would render the mini panel with the name `my_mini_panel`. This mini panel should not have any required contexts.

`[node:panels_mini:my_mini_panels_node]`
> This token would render a mini panel with the name `my_mini_panels_node`. This mini panel can have one required context on type `Node`.
> Entities of any type can be passed to a mini panel this way too.